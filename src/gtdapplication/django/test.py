import os

DEBUG = True

ALLOWED_HOSTS = ['localhost', '127.0.0.1']

REMOTE_LOGIN_URL = 'test-login-remote-url'

REMOTE_USERS_PASSWORD = 'RemoTeUserPassword'

JWT_PUBLIC_KEY = 'JWT_PUBLIC_KEY'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.getenv('DB_NAME', 'gtd'),
        'USER': os.getenv('DB_USER', 'postgres'),
        'PASSWORD': os.getenv('DB_PASS', 'postgres'),
        'HOST': os.getenv('DB_HOST', 'localhost'),
        'PORT': os.getenv('DB_PORT', '5432'),
    }
}
