from .django.base import *

if ENV == 'prod':
    from .django.prod import *
elif ENV == 'test':
    from .django.test import *
elif ENV == 'local':
    from .django.local import *
