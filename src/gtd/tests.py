import json
import copy
from unittest.mock import patch

import jwt
from django.urls import reverse
from django.conf import settings
from rest_framework.test import APITestCase

from .test_data import task_roster_data, task_data


@patch.object(jwt, 'decode', return_value=dict(sub=dict(id=3, is_stuff=True)))
class GTDTasksRosterAPITestCase(APITestCase):

    def setUp(self) -> None:
        self.token = 'Authorization-token'
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

    def create_task_roster(self):
        return self.client.post(reverse('tasksroster-list'), task_roster_data, format='json')

    def test_task_roster_create(self, mock_jwt_decode):
        resp = self.create_task_roster()
        self.assertEqual(resp.status_code, 201)
        mock_jwt_decode.assert_called_once_with(self.token, settings.JWT_PUBLIC_KEY, algorithms='RS256')

    def test_task_roster_list(self, mock_jwt_decode):
        resp = self.create_task_roster()
        self.assertEqual(resp.status_code, 201)

        resp = self.create_task_roster()
        self.assertEqual(resp.status_code, 201)

        resp = self.client.get(reverse('tasksroster-list'), format='json')
        data = json.loads(resp.content)['results']
        self.assertEqual(len(data), 2)

    def test_task_roster_detail(self, mock_jwt_decode):
        resp = self.create_task_roster()
        self.assertEqual(resp.status_code, 201)

        resp = self.client.get(reverse('tasksroster-list'), format='json')
        self.assertEqual(resp.status_code, 200)
        data = json.loads(resp.content)['results']
        tasks_roster_id = data[0]['id']

        url = reverse('tasksroster-detail', kwargs={'pk': tasks_roster_id})
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

    def test_task_roster_patch_update(self, mock_jwt_decode):
        resp = self.create_task_roster()
        self.assertEqual(resp.status_code, 201)

        resp = self.client.get(reverse('tasksroster-list'), format='json')
        self.assertEqual(resp.status_code, 200)
        data = json.loads(resp.content)['results']
        tasks_roster_id = data[0]['id']
        tasks_roster_title = data[0]['title']
        self.assertEqual(tasks_roster_title, task_roster_data['title'])

        url = reverse('tasksroster-detail', kwargs={'pk': tasks_roster_id})
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

        new_data = {
            'title': 'abcd'
        }
        resp = self.client.patch(url, new_data, format='json')
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get(reverse('tasksroster-list'), format='json')
        data = json.loads(resp.content)['results']
        tasks_roster_title = data[0]['title']
        self.assertEqual(tasks_roster_title, new_data['title'])

    def test_task_roster_put_update(self, mock_jwt_decode):
        resp = self.create_task_roster()
        self.assertEqual(resp.status_code, 201)

        resp = self.client.get(reverse('tasksroster-list'), format='json')
        self.assertEqual(resp.status_code, 200)

        data = json.loads(resp.content)['results']
        tasks_roster_id = data[0]['id']
        tasks_roster_title = data[0]['title']
        self.assertEqual(tasks_roster_title, task_roster_data['title'])

        url = reverse('tasksroster-detail', kwargs={'pk': tasks_roster_id})
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

        new_data = {
            'title': 'New task roster title.'
        }
        resp = self.client.put(url, new_data, format='json')
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get(reverse('tasksroster-list'), format='json')
        data = json.loads(resp.content)['results']
        tasks_roster_title = data[0]['title']
        self.assertEqual(tasks_roster_title, new_data['title'])

    def test_task_roster_delete(self, mock_jwt_decode):
        resp = self.create_task_roster()
        self.assertEqual(resp.status_code, 201)

        resp = self.client.get(reverse('tasksroster-list'), format='json')
        data = json.loads(resp.content)['results']
        tasks_roster_id = data[0]['id']
        url = reverse('tasksroster-detail', kwargs={'pk': tasks_roster_id})
        resp = self.client.delete(url)
        self.assertEqual(resp.status_code, 204)

        resp = self.client.get(reverse('tasksroster-list'), format='json')
        data = json.loads(resp.content)['results']
        self.assertEqual(data, [])


@patch.object(jwt, 'decode', return_value=dict(sub=dict(id=1, is_stuff=True)))
class GTDTaskAPITestCase(APITestCase):
    fixtures = ['tasksroster.json']

    def setUp(self) -> None:
        self.token = 'Authorization-token'
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

    def create_task(self):
        return self.client.post(reverse('task-list'), task_data, format='json')

    def test_task_create(self, mock_jwt_decode):
        resp = self.create_task()
        self.assertEqual(resp.status_code, 201)
        mock_jwt_decode.assert_called_once_with(self.token, settings.JWT_PUBLIC_KEY, algorithms='RS256')

    def test_task_list(self, mock_jwt_decode):
        resp = self.create_task()
        self.assertEqual(resp.status_code, 201)

        resp = self.create_task()
        self.assertEqual(resp.status_code, 201)

        resp = self.create_task()
        self.assertEqual(resp.status_code, 201)

        resp = self.client.get(reverse('task-list'), format='json')
        data = json.loads(resp.content)['results']
        self.assertEqual(len(data), 3)

    def test_task_detail(self, mock_jwt_decode):
        resp = self.create_task()
        self.assertEqual(resp.status_code, 201)

        resp = self.client.get(reverse('task-list'), format='json')
        self.assertEqual(resp.status_code, 200)
        data = json.loads(resp.content)['results']
        task_id = data[0]['id']

        url = reverse('task-detail', kwargs={'pk': task_id})
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

    def test_task_patch_update(self, mock_jwt_decode):
        resp = self.create_task()
        self.assertEqual(resp.status_code, 201)

        resp = self.client.get(reverse('task-list'), format='json')
        self.assertEqual(resp.status_code, 200)
        data = json.loads(resp.content)['results']
        task_id = data[0]['id']
        task_title = data[0]['title']
        self.assertEqual(task_title, task_data['title'])

        url = reverse('task-detail', kwargs={'pk': task_id})

        new_data = {
            'title': 'new task title',
            'tasksroster': 1,
            'description': 'new task description',
            'priority': 0,
            'status': False
        }
        resp = self.client.patch(url, new_data, format='json')
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get(reverse('task-list'), format='json')
        data = json.loads(resp.content)['results']
        self.assertEqual(data[0]['title'], new_data['title'])
        self.assertEqual(data[0]['description'], new_data['description'])

    def test_task_put_update(self, mock_jwt_decode):
        resp = self.create_task()
        self.assertEqual(resp.status_code, 201)

        resp = self.client.get(reverse('task-list'), format='json')
        self.assertEqual(resp.status_code, 200)
        data = json.loads(resp.content)['results']
        task_id = data[0]['id']
        task_title = data[0]['title']
        self.assertEqual(task_title, task_data['title'])

        url = reverse('task-detail', kwargs={'pk': task_id})

        new_data = {
            'title': 'new task title',
            'tasksroster': 1,
            'description': 'new task description',
            'priority': 100,
            'status': False
        }
        resp = self.client.put(url, new_data, format='json')
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get(reverse('task-list'), format='json')
        data = json.loads(resp.content)['results']
        self.assertEqual(data[0]['priority'], new_data['priority'])
        self.assertEqual(data[0]['title'], new_data['title'])

    def test_task_delete(self, mock_jwt_decode):
        resp = self.create_task()
        self.assertEqual(resp.status_code, 201)

        resp = self.client.get(reverse('task-list'), format='json')
        data = json.loads(resp.content)['results']
        task_id = data[0]['id']
        url = reverse('task-detail', kwargs={'pk': task_id})
        resp = self.client.delete(url)
        self.assertEqual(resp.status_code, 204)

        resp = self.client.get(reverse('task-list'), format='json')
        data = json.loads(resp.content)['results']
        self.assertEqual(data, [])


class PermissionsAPITestCase(APITestCase):
    fixtures = ['initial_data.json']

    def setUp(self) -> None:
        self.token = 'Authorization-token'
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

    @patch.object(jwt, 'decode', return_value=dict(sub=dict(id=1, is_stuff=True)))
    def test_permissions_tasks_roster_access(self, mock_jwt_decode):
        resp = self.client.get(reverse('tasksroster-detail', kwargs={'pk': 1}))
        self.assertEqual(resp.status_code, 200)
        mock_jwt_decode.assert_called_once_with(self.token, settings.JWT_PUBLIC_KEY, algorithms='RS256')

        resp = self.client.get(reverse('tasksroster-list'))
        self.assertEqual(resp.status_code, 200)
        data = json.loads(resp.content)['results']
        self.assertEqual(len(data), 2)

    @patch.object(jwt, 'decode', return_value=dict(sub=dict(id=2, is_stuff=True)))
    def test_permissions_tasks_roster_access_denied(self, mock_jwt_decode):
        resp = self.client.get(reverse('tasksroster-detail', kwargs={'pk': 1}))
        self.assertEqual(resp.status_code, 404)
        mock_jwt_decode.assert_called_once_with(self.token, settings.JWT_PUBLIC_KEY, algorithms='RS256')

        resp = self.client.get(reverse('tasksroster-list'))
        self.assertEqual(resp.status_code, 200)
        data = json.loads(resp.content)['results']
        self.assertEqual(len(data), 1)

    @patch.object(jwt, 'decode', return_value=dict(sub=dict(id=1, is_stuff=True)))
    def test_permissions_task_access(self, mock_jwt_decode):
        resp = self.client.get(reverse('task-detail', kwargs={'pk': 1}))
        self.assertEqual(resp.status_code, 200)
        mock_jwt_decode.assert_called_once_with(self.token, settings.JWT_PUBLIC_KEY, algorithms='RS256')

        resp = self.client.get(reverse('task-list'))
        self.assertEqual(resp.status_code, 200)
        data = json.loads(resp.content)['results']
        self.assertEqual(len(data), 6)

    @patch.object(jwt, 'decode', return_value=dict(sub=dict(id=3, is_stuff=True)))
    def test_permissions_task_access_denied(self, mock_jwt_decode):
        resp = self.client.get(reverse('task-detail', kwargs={'pk': 1}))
        self.assertEqual(resp.status_code, 404)
        mock_jwt_decode.assert_called_once_with(self.token, settings.JWT_PUBLIC_KEY, algorithms='RS256')

        resp = self.client.get(reverse('task-list'))
        self.assertEqual(resp.status_code, 200)
        data = json.loads(resp.content)['results']
        self.assertEqual(len(data), 3)


class FilteringAPITestCase(APITestCase):
    fixtures = ['initial_data.json']

    def setUp(self) -> None:
        self.token = 'Authorization-token'
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

    @patch.object(jwt, 'decode', return_value=dict(sub=dict(id=1, is_stuff=True)))
    def test_task_list_ordering_priority(self, mock_jwt_decode):
        params = {
            'ordering': 'priority'
        }
        resp = self.client.get(reverse('task-list'), params, format='json')
        data = json.loads(resp.content)['results']
        self.assertEqual(len(data), 6)
        self.assertEqual(data[0]['description'], 'lower task')
        self.assertEqual(data[0]['id'], 1)
        self.assertEqual(data[1]['id'], 10)
        self.assertEqual(data[1]['description'], 'lower task')
        self.assertEqual(data[2]['id'], 2)
        self.assertEqual(data[2]['description'], 'mid task')
        self.assertEqual(data[3]['id'], 11)
        self.assertEqual(data[3]['description'], 'middle task')
        self.assertEqual(data[4]['id'], 3)
        self.assertEqual(data[4]['description'], 'top task')
        self.assertEqual(data[5]['id'], 12)
        self.assertEqual(data[5]['description'], 'top task')
