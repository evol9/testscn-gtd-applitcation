from rest_framework import serializers
from rest_framework.exceptions import PermissionDenied

from .models import TasksRoster, Task


class TaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = Task
        fields = ('id', 'title', 'added', 'tasksroster', 'description',
                  'priority', 'status')

    def validate(self, data):
        tasksroster = data['tasksroster']
        if tasksroster.user_id == self.context['request'].user.id:
            return super().validate(data)
        raise PermissionDenied


class TasksRosterSerializer(serializers.ModelSerializer):
    user_id = serializers.ReadOnlyField()

    class Meta:
        model = TasksRoster
        fields = ('id', 'title', 'user_id')
