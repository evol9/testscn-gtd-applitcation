from rest_framework import viewsets, status, filters
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from django_filters.rest_framework import DjangoFilterBackend

from .models import Task, TasksRoster
from .serializers import TasksRosterSerializer, TaskSerializer
from .permissions import AllowOptionsAuthentication


class TaskViewSet(viewsets.ModelViewSet):
    serializer_class = TaskSerializer
    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]
    filterset_fields = ['tasksroster', 'priority', 'status']
    ordering_fields = ('priority', 'title')

    def get_queryset(self):
        return Task.objects.filter(tasksroster__user_id=self.request.user.id).order_by('-added').select_related()

    # def get_serializer_context(self):
    #     return {'request': self.request}

    def check_create_update_permissions(self, request, validated_serializer_data):
        for permission in self.get_permissions():
            if not permission.has_create_update_permissions(request, self, validated_serializer_data):
                self.permission_denied(request)


class TasksRosterViewSet(viewsets.ModelViewSet):
    serializer_class = TasksRosterSerializer
    permission_classes = (AllowOptionsAuthentication,)

    def get_queryset(self):
        return TasksRoster.objects.filter(user_id=self.request.user.id).order_by('id')

    def perform_create(self, serializer):
        serializer.save(user_id=self.request.user.id)


@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'Tasks': reverse('task-list', request=request, format=format),
        'TasksRoters': reverse('tasksroster-list', request=request,
                              format=format)
    })
