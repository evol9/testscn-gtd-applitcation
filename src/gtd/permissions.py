from rest_framework import permissions


class AllowOptionsAuthentication(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method in ['OPTIONS']:
            return True
        return request.user and request.user.is_authenticated
