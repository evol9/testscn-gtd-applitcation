from django.db import models


class TasksRoster(models.Model):
    user_id = models.IntegerField(blank=False)
    title = models.CharField(max_length=120)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ["title"]


class Task(models.Model):
    tasksroster = models.ForeignKey(TasksRoster, on_delete=models.CASCADE)
    title = models.CharField(max_length=120)
    description = models.TextField()
    priority = models.IntegerField(default=0)
    status = models.BooleanField(default=False)
    added = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-id']
