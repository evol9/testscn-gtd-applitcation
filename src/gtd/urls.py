from django.urls import path, include
from rest_framework.routers import DefaultRouter

from gtd import views


router = DefaultRouter()
router.register(r'tasks', views.TaskViewSet, basename='task')
router.register(r'tasksrosters', views.TasksRosterViewSet,
                basename='tasksroster')


urlpatterns = [
    path('api/v1/', include(router.urls)),
]
