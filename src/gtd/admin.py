from django.contrib import admin

from .models import TasksRoster, Task


class TaskInline(admin.TabularInline):
    model = Task


class TaskAdmin(admin.ModelAdmin):
    list_filter = ('title', 'description', 'tasksroster',
                   'priority', 'status', 'added')
    search_fields = ('title', 'description', 'tasksroster',
                   'priority', 'status', 'added')


class TasksRosterAdmin(admin.ModelAdmin):
    list_filter = ('title', 'user_id')
    search_fields = ('title', 'user_id')
    inlines = (TaskInline, )


admin.site.register(Task, TaskAdmin)
admin.site.register(TasksRoster, TasksRosterAdmin)
