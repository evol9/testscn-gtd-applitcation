import logging

import requests
import jwt
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.conf import settings

UserModel = get_user_model()


class APIBackend(requests.Session):

    def __init__(self):
        super().__init__()
        self.token = None
        self.id = None

    def authenticate(self, request, username=None, password=None):

        if not username or not password:
            return

        if not self.token:
            self.id, self.token = self.remote_login(username, password)

        if not self.is_valid_remote_token(self.token):
            self.id, self.token = self.remote_login(username, password)
            if not self.is_valid_remote_token(self.token):
                return

        user_qs = UserModel.objects.filter(username=username)
        user = user_qs.first() if user_qs.exists() else \
            UserModel.objects.create_superuser(username, username, settings.REMOTE_USERS_PASSWORD)

        return user

    def remote_login(self, email, passw):
        data = {
            'email': email,
            'password': passw
        }
        try:
            resp = self.post(settings.REMOTE_LOGIN_URL, json=data)
        except requests.ConnectionError:
            logging.error('Authentication server is down.')
            raise ValidationError('Authentication server is down.')

        if resp and resp.status_code == 200:
            token = resp.json()['auth_token']
            user_id = resp.json()['id']
            return user_id, token
        return None, None

    @staticmethod
    def is_valid_remote_token(auth_token):
        is_stuff = False
        if not auth_token:
            return

        try:
            payload = jwt.decode(
                auth_token,
                settings.JWT_PUBLIC_KEY,
                algorithms='RS256'
            )
            is_stuff = payload['sub']['is_stuff']
        except jwt.ExpiredSignatureError:
            logging.info('Signature expired.')
        except jwt.InvalidTokenError:
            logging.info('Invalid token. Check is Xryptography installed.')

        return is_stuff

    @staticmethod
    def get_user(user_id):
        try:
            return UserModel.objects.get(pk=user_id)
        except UserModel.DoesNotExist:
            return None

