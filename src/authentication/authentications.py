import logging

import jwt
from rest_framework import authentication, exceptions
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser


class Authentication(authentication.BaseAuthentication):

    def authenticate(self, request):

        if "Authorization" not in request.headers:
            return AnonymousUser(), None

        try:
            auth_header = request.headers.get('Authorization', '')
            auth_token = auth_header.split(' ')[1]
            if not auth_token:
                return AnonymousUser(), None

            try:
                payload = jwt.decode(
                    auth_token,
                    settings.JWT_PUBLIC_KEY,
                    algorithms='RS256'
                )
            except jwt.ExpiredSignatureError:
                logging.info("Signature expired.")
                return AnonymousUser(), None
            except jwt.InvalidTokenError:
                logging.info("Invalid token.")
                return AnonymousUser(), None

            User = get_user_model()
            remote_user = User('User', '', 'qawsedrf')
            remote_user.id = payload['sub']['id']
            remote_user.is_stuff = payload['sub']['is_stuff']
            remote_user.is_superuser = True if remote_user.is_stuff else False
            request.user = remote_user
        except IndexError:
            return AnonymousUser(), None

        return remote_user, None


class TestAuthentication(authentication.BaseAuthentication):

    def authenticate(self, request):
        User = get_user_model()
        remote_user = User('User', '', 'qawsedrf')
        remote_user.id = 1
        remote_user.is_stuff = True
        remote_user.is_superuser = True
        request.user = remote_user
        return remote_user, None
