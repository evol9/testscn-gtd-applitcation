from unittest.mock import patch

import jwt
from django.urls import reverse
from django.conf import settings
from rest_framework.test import APITestCase
from django.test import Client
from django.test.client import MULTIPART_CONTENT
from django.contrib.auth.models import User

from authentication.backends import APIBackend


class APIAuthenticationTestCase(APITestCase):

    @patch.object(jwt, 'decode', return_value=dict(sub=dict(id=3, is_stuff=True)))
    def test_remote_api_authentication(self, mock_jwt_decode):
        auth_token = 'token'
        data = {
            'title': 'abc'
        }
        resp = self.client.post(reverse('tasksroster-list'), data=data, HTTP_AUTHORIZATION=f'Bearer {auth_token}')
        mock_jwt_decode.assert_called_once_with(auth_token, settings.JWT_PUBLIC_KEY, algorithms='RS256')
        self.assertEqual(resp.status_code, 201)


class AuthenticationTestCase(APITestCase):

    @patch.object(APIBackend, 'remote_login', return_value=tuple([2, 'token']))
    @patch.object(jwt, 'decode', return_value=dict(sub=dict(id=2, is_stuff=False)))
    def test_remote_authentication_not_stuff(self, mocked_jwt_decode, mocked_remote_login):
        credentionals = {
            'username': 'username@user.com',
            'password': 'testpassword'
        }
        resp = self.client.post(reverse('admin:login'), data=credentionals)
        self.assertTrue(resp.context['user'].is_anonymous)
        mocked_jwt_decode.assert_called_with('token', settings.JWT_PUBLIC_KEY, algorithms='RS256')
        mocked_remote_login.assert_called_with(credentionals['username'], credentionals['password'])

    @patch.object(APIBackend, 'remote_login', return_value=tuple([2, 'token']))
    @patch.object(jwt, 'decode', return_value=dict(sub=dict(id=2, is_stuff=True)))
    def test_remote_authentication_stuff(self, mocked_jwt_decode, mocked_remote_login):
        users_count = User.objects.all().count()
        c = Client()
        credentials = {
            'username': 'username@user.com',
            'password': 'testpassword'
        }
        resp = c.post(
            reverse('admin:login'),
            credentials,
            content_type=MULTIPART_CONTENT,
        )
        self.assertEqual(resp.status_code, 302)
        self.assertFalse(resp.wsgi_request.user.is_anonymous)
        self.assertTrue(resp.wsgi_request.user.is_authenticated)
        self.assertRedirects(resp, reverse('admin:index'), status_code=302, target_status_code=200)
        mocked_jwt_decode.assert_called_with('token', settings.JWT_PUBLIC_KEY, algorithms='RS256')
        mocked_remote_login.assert_called_with(credentials['username'], credentials['password'])
        self.assertEqual(users_count + 1, User.objects.all().count())
